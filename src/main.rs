#![feature(plugin)]
#![plugin(rocket_codegen)]
 
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate serde_json;
extern crate blake2;
extern crate rocket;
extern crate rocket_contrib;
extern crate uuid;
extern crate url;
extern crate reqwest;


pub mod bchain;
use std::sync::Mutex;
use bchain::blockchain  ::{BlockChain, Transaction, hash_block};
use rocket              ::{State};
use rocket_contrib      ::{Json, Value};
use uuid::Uuid;


type SharedBlockChain = Mutex<BlockChain>;

#[get("/mine")]
fn mine(shared_blockchain : State<SharedBlockChain>, uuid : State<Uuid>) -> Json<Value> {
    let mut blockchain  = shared_blockchain.lock().unwrap();
    let last_block      = blockchain.chain.last().unwrap().clone();
    let last_proof      = last_block.proof.clone();
    let proof           = blockchain.proof_of_work(&last_proof);
    blockchain.new_transaction(
        Transaction{
            sender      : "0".to_string(),
            recipient   : uuid.simple().to_string(),
            amount      : 1
        }
    );
    let prev_hash   = hash_block(&last_block);
    let block       = blockchain.new_block(proof, Some(prev_hash));
    Json(json!({
        "status"    : 200,
        "message"   : "New block was forged",
        "block"     : block
    }))
}

#[post("/transactions/new", format = "application/json", data = "<transaction>")]
fn new_transaction(
    transaction         : Json<Transaction>, 
    shared_blockchain   : State<SharedBlockChain>
    ) -> Json<Value> {
    let mut blockchain  = shared_blockchain.lock().unwrap();
    let block_index     = blockchain.new_transaction(
        transaction.into_inner()
        );
    Json(json!({
        "status"    : 200,
        "message"   : format!("A new transaction will be added to block {}", block_index)
    }))
}


#[get("/chain")]
fn get_chain(shared_blockchain : State<SharedBlockChain>) -> Json<Value> {
    let blockchain  = shared_blockchain.lock().unwrap();
    Json(json!({
        "status"    : 200,
        "length"    : blockchain.chain.len(), 
        "chain"     : blockchain.chain
    }))
}

#[post("/nodes/register", format = "application/json", data = "<json_nodes>")]
fn register_nodes(
    json_nodes : Json<Vec<String>>,
    shared_blockchain : State<SharedBlockChain>
    ) -> Json<Value> {
    let nodes = json_nodes.into_inner();
    if nodes.len() == 0 {
        return Json(json!({
        "status"    : 400,
        "message"   : "Error: Please supply a valid list of nodes"
        }));
    }
    let mut blockchain  = shared_blockchain.lock().unwrap();
    for node in nodes {
        blockchain.register_node(node);
    }
    Json(json!({
        "status"  : 200,
        "nodes"   : blockchain.nodes.clone()
    }))
}

#[get("/nodes/resolve")]
fn consensus(shared_blockchain : State<SharedBlockChain>) ->  Json<Value> {
    let mut blockchain = shared_blockchain.lock().unwrap();
    let result = blockchain.resolve_conflicts();
    let message = match result {
        true    => "Chain was replaced",
        false   => "Chain was authoritative"
    };
    Json(json!({
        "status"    : 200,
        "message"   : message,
        "chain"     : blockchain.chain.clone()
    }))
}

fn main() {
    rocket::ignite()
        .manage(Mutex::new(BlockChain::new()))
        .manage(Uuid::new_v4())
        .mount("/", routes![
            mine, 
            new_transaction, 
            get_chain,
            consensus,
            register_nodes
            ])
        .launch();
}
