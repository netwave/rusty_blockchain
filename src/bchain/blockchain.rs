extern crate serde;
extern crate serde_json;
extern crate blake2;
extern crate url;
extern crate reqwest;

use std::collections::HashSet;

use std::time   ::{SystemTime, UNIX_EPOCH};
use blake2      ::{Blake2b, Digest};
use url         ::{Url};

pub fn hello() -> String {
    "HI!!".to_string()
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Transaction {
    pub sender      : String,
    pub recipient   : String,
    pub amount      : u64
}

#[derive(Debug,Clone, Serialize, Deserialize)]
pub struct Block {
    pub index           : u64,
    pub time_stamp      : u64,
    pub transaction     : Vec<Transaction>,
    pub proof           : u64,
    pub previous_hash   : String
}

#[derive(Debug, Clone)]
pub struct BlockChain {
    pub chain           : Vec<Block>,
        transactions    : Vec<Transaction>,
    pub nodes           : HashSet<String>
}

fn hash_string(s : &String) -> String {
    let res = Blake2b::digest(s.as_bytes());
    format!("{:x}", res)
}

pub fn hash_block(block : &Block) -> String {
    let res = Blake2b::digest(
        serde_json::to_string(block).unwrap().as_bytes()
        );
    format!("{:x}", res)
}

fn valid_proof(a:  &u64, b: &u64) -> bool {
    let guess   = format!("{}{}", a, b);
    let res     = hash_string(&guess);
    let res_len = res.len();
    match res[(res_len-4)..].as_ref() {
        "0000"  => true,
        _       => false
    }
}

impl BlockChain {
    pub fn new() -> BlockChain {
        let mut block_chain = BlockChain{
            chain        : Vec::new(),
            transactions : Vec::new(),
            nodes        : HashSet::new()
            };
        block_chain.new_block(1, Some("".to_string()));
        block_chain
    }

    pub fn new_block(&mut self, proof : u64, prev_hash : Option<String>) -> &Block {
        let i               = self.chain.len() as u64 + 1;
        let previous_hash   = match prev_hash {
                Some(x) => x,
                None    => hash_block(self.chain.last().unwrap())
            };
        self.chain.push(Block{
            index           : i,
            time_stamp      : SystemTime::now()
                            .duration_since(UNIX_EPOCH).unwrap().as_secs() as u64,
            transaction     : self.transactions.clone(),
            proof           : proof,
            previous_hash   : previous_hash
        });
        self.transactions.clear();
        self.chain.last().unwrap()
    }

    pub fn new_transaction (
        &mut self, 
        transaction : Transaction) -> u64 {
        self.transactions.push(transaction);
        self.chain.len() as u64 + 1
    }

    pub fn proof_of_work(&mut self, last_proof : &u64) -> u64 {
        let mut proof : u64 = 0;
        while !valid_proof(last_proof, &proof){
            proof += 1;
        }
        proof
    }

    pub fn register_node(&mut self, address : String) {
        let url = Url::parse(&address).unwrap();
        self.nodes.insert(
            format!(
                "{}:{}",
                url.host().unwrap().to_string(),
                url.port().unwrap().to_string())
            );
    }

    pub fn valid_chain(&mut self, chain : &Vec<Block>) -> bool {
        let mut last_block : Block = chain.first().unwrap().clone();
        for block in &chain[1..] {
            if block.previous_hash != hash_block(&last_block) {
                return false
            }
            if !valid_proof(&last_block.proof, &block.proof) {
                return false
            }
            last_block = block.clone();
        }
        true
    }

    pub fn resolve_conflicts(&mut self) -> bool {
        #[derive(Deserialize)]
        struct ResponseData {
            status  : u32,
            length  : u64,
            chain   : Vec<Block>
        }
        let neightbours     = self.nodes.clone();
        let mut max_len     = self.chain.len() as u64;
        let mut new_chain : Option<Vec<Block>> = None;
        for node in neightbours {
            let address      = format!("http://{}/chain" , node);
            let mut response = reqwest::get(&address).unwrap();
            if response.status().is_success() {
                let data : ResponseData = response.json().unwrap();
                if data.length > max_len && self.valid_chain(&data.chain) {
                    max_len = data.length;
                    new_chain = Some(data.chain.clone());
                }
            }
        }
        match new_chain {
            Some(chain) => {
                self.chain = chain;
                true
            }
            _ => false
        }
    }
}




